import Vue from 'vue'
import Router from 'vue-router'
import Country from '@/components/Country'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Country',
      component: Country
    }
  ]
})
